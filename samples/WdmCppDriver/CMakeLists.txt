
set(MY_TEST_CERT MyTestCert)

# set(WINSDK ${WDK_ROOT}/bin/${WDK_VERSION}/x86)
set(OPENSSL "openssl.exe")
set(MAKECERT "makecert.exe")
set(CERTUTIL "certutil.exe")
set(CERTMGR "certmgr.exe")
set(CERT2SPC "cert2spc.exe")
set(PVK2PFX "pvk2pfx.exe")
set(SIGNTOOL "signtool.exe")

add_custom_command(OUTPUT ${MY_TEST_CERT}.pfx
	COMMAND "${CMAKE_COMMAND}" -E remove ${MY_TEST_CERT}.pvk ${MY_TEST_CERT}.cer ${MY_TEST_CERT}.pfx ${MY_TEST_CERT}.spc
	COMMAND "${MAKECERT}" -b 09/09/2018 -r -n \"CN=TestWare\" -sv ${MY_TEST_CERT}.pvk ${MY_TEST_CERT}.cer
	COMMAND "${CERTMGR}" -add ${MY_TEST_CERT}.cer -s -r localMachine ROOT
	COMMAND "${CERTMGR}" -add ${MY_TEST_CERT}.cer -s -r localMachine TRUSTEDPUBLISHER
	COMMAND "${CERT2SPC}" ${MY_TEST_CERT}.cer ${MY_TEST_CERT}.spc
	COMMAND "${PVK2PFX}" -pvk ${MY_TEST_CERT}.pvk -spc ${MY_TEST_CERT}.spc -pfx ${MY_TEST_CERT}.pfx
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
	COMMENT "Generating SSL certificates to sign the drivers and executable ...")


add_custom_target(${MY_TEST_CERT}
	DEPENDS ${MY_TEST_CERT}.pfx)



wdk_add_driver(WdmCppDriver
    Main.cpp
    )

add_dependencies(WdmCppDriver ${MY_TEST_CERT})
target_link_libraries(WdmCppDriver WdmCppLib)


add_custom_command(TARGET WdmCppDriver
	POST_BUILD
	COMMAND "${SIGNTOOL}" sign /v /f ../${MY_TEST_CERT}.pfx /t http://timestamp.digicert.com /fd certHash $<TARGET_FILE:WdmCppDriver>
        COMMENT "Signing $<TARGET_FILE:${USBSNOOP} ...")
